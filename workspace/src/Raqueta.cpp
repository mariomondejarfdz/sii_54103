// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"
#include "glut.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	y1 = y1 + t*velocidad.y;
	y2 = y2 + t*velocidad.y;
}

void Raqueta::DibujaDisparo()
{
	glColor3ub(255,0,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(disparo.centro.x,disparo.centro.y,0);
	glutSolidSphere(disparo.radio,15,15);
	glPopMatrix();
}
void Raqueta::MueveDisparo(float t)
{
	disparo.centro.x = disparo.centro.x + 0.1*t;	
}
