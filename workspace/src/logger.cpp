#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <string>


int main(int argc,char* argv[])
{
	int fd;
	char mensaje[200];
	unlink("/tmp/FIFO");
	if ((mkfifo("/tmp/FIFO", 0777))<0){
		perror("No puede crearse el FIFO");
		return(1);
	}
	
	if ((fd=open("/tmp/FIFO", O_RDONLY))<0){
		perror("No puede crearse el FIFO");
		return(1);
	}
	while(1)
	{
		read(fd, &mensaje, sizeof(mensaje));
		printf("%s\n", mensaje);
	}
	close(fd);
	unlink("/tmp/FIFO");
	return(0);


}
