# Changelog
All notable changes to this project will be documented in this file.

## [1.0.0] - 2022-10-06
### Added
- Changelog file.

## [1.0.1] - 2022-10-06
### Added
- Movimiento de las raquetas y la esfera.

## [1.0.2] - 2022-10-07
### Added
- Reducción del tamaño de la esfera.

## [1.0.3] - 2022-10-08
### Added
- Disparos con las teclas E y O.
- Al impactar las raquetas se hacen mas pequeñas.

## [1.0.3] - 2022-10-08
### Added
- Logger

## [1.2.0] - 2022-14-12
### Added
- Envio de coordenadas desde el servidor al cliente


